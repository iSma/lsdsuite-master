import logging
import time
import json

from .churn import Churn

log = logging.getLogger(__name__)


class Benchmark(object):
    def __init__(self, engine, config, name, spec, churn=None,
                 run_time=None, start_time=0, end_time=0):
        from datetime import datetime
        self.date = datetime.now().replace(microsecond=0)

        self.engine = engine
        self.config = config  # TODO: check config
        self.name = name
        self.spec = spec

        if not churn and not run_time:
            raise ValueError("Either churn or run_time must be specified!")

        if churn:
            churn = Churn(churn)

        self.churn = churn
        self.run_time = run_time
        self.start_time = start_time
        self.end_time = end_time

        self.app = None
        self.run = 0

    @property
    def log_file(self):
        return ("{date}--{name}--run-{run}.log"
                .format(date=self.date.isoformat(),
                        name=self.name,
                        run=self.run))

    @property
    def results_dir(self):
        from os.path import join
        return join("{date}--{name}".format(date=self.date.isoformat(),
                                            name=self.name),
                    "run-{run}".format(run=self.run))

    @property
    def mark(self):
        return {
            "type": "benchmark",
            "name": self.name,
            "run": self.run
        }

    def start(self):
        """Runs one run of the benchmark."""
        if self.app is not None:
            log.info("%s already started", self)
            return

        self.run += 1
        log.info("%s: starting run %d", self, self.run)
        # TODO: make this more engine-agnostic
        # maybe move it to engine.create_app
        self.engine.send('log', file=self.log_file)

        msg = dict(self.mark, status="start")
        self.engine.send('mark', msg=json.dumps(msg))

        spec = self.spec
        if self.churn:
            self.churn.apply_start(spec)

        self.app = self.engine.create_app(self.name, spec)
        log.info("%s: waiting %ds before start", self, self.start_time)
        time.sleep(self.start_time)

        if self.churn:
            log.info("%s: starting churn", self)
            self.churn.start(self.engine, self.app)
        else:
            log.info("%s: running for %ds", self, self.run_time)
            time.sleep(self.run_time)

        log.info("%s: waiting %ds before end", self, self.end_time)
        time.sleep(self.end_time)
        self.stop()

    def stop(self, wait=30, get_logs=True):
        if self.app is None:
            return

        self.app.remove()
        log.info("%s: waiting %ds for services to shut down", self, wait)
        time.sleep(wait)
        self.app = None

        msg = dict(self.mark, status="stop")
        self.engine.send('mark', msg=json.dumps(msg))
        self.engine.send('log', file=None)

        log.info("%s: end of run %d", self, self.run)

        if get_logs:
            self.get_logs()

    def get_logs(self):
        from os.path import abspath, join
        from glob import iglob
        from subprocess import run

        log.info("%s: fetching logs", self)
        for i, node in enumerate(self.config['nodes']):
            self._get_logs(node, i)

        # merge
        path = abspath(self.config['results_dir'])
        path = join(path, self.results_dir)

        out = join(path, "out.log")
        pat = join(path, "nodes", "*.log")

        run(["sort", "-o", out, "-m", *iglob(pat)], check=True)

    def _get_logs(self, node, i):
        from os import makedirs
        from os.path import abspath, join

        remote_path = abspath(self.config['lsdsuite_dir'])
        remote_path = join(remote_path, "logs", self.log_file)

        local_path = abspath(self.config['results_dir'])
        local_path = join(local_path, self.results_dir, "nodes")
        makedirs(local_path, exist_ok=True)
        local_path = join(local_path, "{i}.log".format(i=i))

        log.debug("%s: node %d: fetching logs from %s to %s",
                  self, i, remote_path, local_path)

        if i == 0 and not node.get('remote'):
            self._get_logs_local(node, local_path, remote_path)
        else:
            self._get_logs_ssh(node, local_path, remote_path)

    def _get_logs_local(self, node, local_path, remote_path):
        from shutil import copy
        copy(remote_path, local_path)

    def _get_logs_ssh(self, node, local_path, remote_path):
        from . import get_ssh

        def progress(i, total):
            log.debug("%s: SSH progress: %3.0f%%", self, 100 * i/total)

        with get_ssh(node) as ssh:
            sftp = ssh.open_sftp()
            sftp.get(remote_path, local_path, callback=progress)

    def __repr__(self):
        return "Benchmark[{name}]".format(name=self.name)
