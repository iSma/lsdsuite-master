import re
import logging
import time

import docker
from docker import types
from docker.errors import APIError

from .. import DEFAULT_CONFIG, get_ssh
log = logging.getLogger(__name__)

# Regex for parsing short-syntax port specification
# e.g.: 8080:80/tcp, 80:8080, 8080 (= 8080:8080)
# TODO: doesn't support advanced features such as port ranges
PORT_REGEX = re.compile(r"^(\d+)(?::(\d+))?(?:/(udp|tcp))?$")


class Engine(object):
    def __init__(self, config=None, client=None):
        from os.path import abspath, join

        if config is None:
            config = {}

        self.config = config = dict(DEFAULT_CONFIG, **config)

        lsdsuite_dir = abspath(config['lsdsuite_dir'])
        self.logs_dir = join(lsdsuite_dir, 'logs')
        self.sock_dir = join(lsdsuite_dir, 'sock')
        self.log_address = 'unix://' + join(self.sock_dir, 'logs.sock')
        self.slave_image = config['slave_image']
        self.slave_port = config['slave_port']
        self.docker_sock = config['docker_sock']

        if not client:
            client = docker.APIClient(version='auto')
        elif type(client) is docker.client.DockerClient:
            client = client.api
        self.client = client

        # Resource cache
        self._cache = {cls: dict() for cls in [Node, Network, Service, Task]}

        # Getter methods for a single resource
        self._get = {
            Node: client.inspect_node,
            Network: client.inspect_network,
            Service: client.inspect_service,
            Task: client.inspect_task
        }

        # Getter methods for resource lists
        self._list = {
            Node: client.nodes,
            Network: client.networks,
            Service: client.services,
            Task: client.tasks
        }

    def list(self, cls, filters=None):
        data = self._list[cls](filters=filters)
        return [self._update_cache(cls, d) for d in data]

    def get(self, cls, id):
        data = self._get[cls](id)
        return self._update_cache(cls, data)

    def nodes(self, **filters):
        # TODO: filter out nodes that aren't ready?
        return self.list(Node, filters)

    def node(self, id):
        return self.get(Node, id)

    def networks(self, **filters):
        return self.list(Network, filters)

    def network(self, id):
        return self.get(Network, id)

    @property
    def managed_networks(self):
        return self.networks(label='org.lsdsuite.app.id')

    def services(self, **filters):
        return self.list(Service, filters)

    def service(self, id):
        return self.get(Service, id)

    @property
    def managed_services(self):
        return self.services(label='org.lsdsuite.app.id')

    def tasks(self, **filters):
        return self.list(Task, filters)

    def task(self, id):
        return self.get(Task, id)

    def send(self, command, **params):
        return [node.send(command, **params) for node in self.nodes()]

    def prune(self):
        log.info("Cleanup:")
        for service in self.managed_services:
            service.remove()
            log.info("Removed %s", service)

        for network in self.managed_networks:
            network.remove()
            log.info("Removed %s", network)

    def update_node(self, node):
        spec = node['Spec']
        self.client.update_node(node.id, node.version, spec)
        node.reload()

    def remove_network(self, id):
        self.client.remove_network(id)

    def remove_service(self, id):
        self.client.remove_service(id)

    def update_service(self, service):
        """Update a service's configuration.

        Mainly used for scaling it up/down.
        """
        spec = service['Spec']
        self.client.update_service(service.id, service.version,
                                   task_template=spec.get('TaskTemplate'),
                                   name=spec.get('Name'),
                                   labels=spec.get('Labels'),
                                   mode=spec.get('Mode'),
                                   update_config=spec.get('UpdateConfig'),
                                   networks=spec.get('Networks'),
                                   endpoint_spec=spec.get('EndpointSpec'))
        service.reload()

    @staticmethod
    def _node_do_ssh(node, cmd, check=True):
        with get_ssh(node) as ssh:
            _, _, stderr = ssh.exec_command(cmd)

            status = stderr.channel.recv_exit_status()
            if status == 0:
                return True

            if check:
                error = ""
                for line in stderr:
                    error += line
                raise APIError(error)

            return False

    def _node_leave_swarm(self, node, check=True):
        cmd = "docker swarm leave --force"
        return self._node_do_ssh(node, cmd, check=check)

    def _node_join_swarm(self, node, manager_ip, token, check=True):
        cmd = "docker swarm join --token '{token}' '{address}'"
        cmd = cmd.format(token=token, address=manager_ip)
        if not self._node_do_ssh(node, cmd, check=check):
            return False
        else:
            return self._node_create_dirs(node, check=check)

    def _node_create_dirs(self, node, check=True):
        cmd = "mkdir -p '{}' '{}'".format(self.logs_dir, self.sock_dir)
        return self._node_do_ssh(node, cmd, check=check)

    def _cluster_is_init(self):
        try:
            self.client.inspect_swarm()
            return True
        except APIError:
            return False

    def cluster_status(self):
        if not self._cluster_is_init():
            return None

        config_nodes = self.config['nodes']
        manager = config_nodes[0]
        swarm_nodes = {n.ip: n for n in self.nodes()}

        status = []
        for node in config_nodes:
            ok = True
            s = {k: None for k in
                 ['address', 'hostname', 'ssh', 'swarm', 'ready', 'slave']}

            ip = s['address'] = node['address']

            if node is manager and not node.get('remote'):
                s['ssh'] = True
            else:
                try:
                    with get_ssh(node):
                        s['ssh'] = True
                except Exception as e:
                    # TODO: catch more specific exception
                    log.warning("Can't reach %s: %s", ip, e)
                    ok = s['ssh'] = False

            n = swarm_nodes.get(ip)
            if n is None:
                ok = s['swarm'] = False
            else:
                s['hostname'] = n.hostname
                s['swarm'] = True
                if n.ready:
                    s['ready'] = True
                else:
                    ok = s['ready'] = False
                try:
                    s['slave'] = n.status
                except Exception as e:
                    # TODO: catch more specific exception
                    log.warning("Can't reach %s: %s", ip, e)
                    ok = s['slave'] = False

            s['ok'] = ok
            status.append(s)

        return status

    def cluster_init(self, force=False):
        from os import makedirs

        manager = self.config['nodes'][0]
        try:
            log.debug("Initializing Swarm @ %s", manager['address'])
            self.client.init_swarm(advertise_addr=manager['address'],
                                   force_new_cluster=force)
        except APIError as e:
            log.error("Couldn't initialize Swarm: %s", e)
            raise e

        if manager.get('remote'):
            self._node_create_dirs(manager, check=True)
        else:
            makedirs(self.sock_dir, exist_ok=True)
            makedirs(self.logs_dir, exist_ok=True)

    def cluster_down(self):
        nodes = self.config['nodes'][1:]

        for node in nodes:
            log.debug("Forcing %s to leave Swarm", node['address'])
            self._node_leave_swarm(node, check=False)

        for node in self.nodes():
            if not node.manager:
                log.debug("Removing %s from Swarm", node.ip)
                self.client.remove_node(node.id, force=True)

        log.debug("Leaving Swarm")
        self.client.leave_swarm(force=True)

    def cluster_up(self, force=False):
        nodes = self.config['nodes']
        manager, nodes = nodes[0], nodes[1:]

        if not self._cluster_is_init():
            self.cluster_init()

        token = self.client.inspect_swarm()
        token = token['JoinTokens']['Worker']

        log.debug("Removing old nodes from Swarm")
        for node in self.nodes():
            if node.ip in [n['address'] for n in nodes + [manager]]:
                if node.ready:
                    continue

            log.debug("Removing %s from Swarm", node.ip)
            self.client.remove_node(node.id, force=force)

        for node in nodes:
            if force:
                log.debug("Forcing %s to leave Swarm", node['address'])
                self._node_leave_swarm(node, check=False)
            elif node['address'] in [n.ip for n in self.nodes()]:
                # Don't re-join node
                continue

            try:
                log.debug("Adding %s to the Swarm", node['address'])
                self._node_join_swarm(node, manager['address'], token)
            except APIError as e:
                log.error("Node %s can't join Swarm: %s", node['address'], e)
                raise e

        self.start_slave(restart=True)
        self.services(name='lsdsuite-slave')[0].wait()

    def start_slave(self, restart=False):
        """Creates and runs lsdsuite-slave service on all nodes."""
        service, = self.services(name='lsdsuite-slave') or [None]
        if service:
            if restart:
                service.remove()
                time.sleep(10)
            else:
                return service

        # Can't do this if slave is down...
        # self.send('pull', image=self.slave_image)

        spec = {
            'image': self.slave_image,
            'environment': {'HOST_PROC': "/app/proc",
                            'HOST_SYS': "/app/sys"},
            'volumes': [{'type': 'bind',
                         'source': self.docker_sock,
                         'target': "/var/run/docker.sock"},
                        {'type': 'bind',
                         'source': self.sock_dir,
                         'target': "/app/sock"},
                        {'type': 'bind',
                         'source': self.logs_dir,
                         'target': "/app/logs"},
                        {'type': 'bind',
                         'read_only': True,
                         'source': "/proc",
                         'target': "/app/proc"},
                        {'type': 'bind',
                         'read_only': True,
                         'source': "/sys",
                         'target': "/app/sys"}],
            'networks': ['host'],  # = --network=host
            'deploy': {
                'mode': 'global',
                'restart_policy': {
                    'condition': 'any',
                    'delay': 5000000000,
                    'max_attempts': 0
                },
            },
            'ports': [{'mode': 'host',
                       'target': 7000,
                       'published': self.slave_port}]
        }

        spec = {'services': {'lsdsuite-slave': spec}}
        args = self._spec_to_service('lsdsuite-slave', spec)
        # Remove logging config
        args['task_template'].pop('LogDriver')

        service = self.client.create_service(**args)
        service = self.service(service['ID'])
        return service

    def stop_slave(self):
        """Stops lsdsuite-slave service."""
        service, = self.services(name='lsdsuite-slave') or [None]
        if service:
            service.remove()

    def start_registry(self, restart=False):
        """Creates and runs registry service."""
        service, = self.services(name='registry') or [None]
        if service:
            if restart:
                service.remove()
                time.sleep(10)
            else:
                return service

        spec = {
            'image': "registry",
            'deploy': {
                'mode': 'replicated',
                'replicas': 1,
                'restart_policy': {
                    'condition': 'any',
                    'delay': 5000000000,
                    'max_attempts': 0
                },
            },
            # TODO: placement node.role==manager
            'ports': [{'target': 5000,
                       'published': 5000}]
        }

        spec = {'services': {'registry': spec}}
        args = self._spec_to_service('registry', spec)
        # Remove logging config
        args['task_template'].pop('LogDriver')

        service = self.client.create_service(**args)
        service = self.service(service['ID'])
        return service

    def create_app(self, name, spec):
        """Creates App from spec.

        Creates all networks and in spec and returns app.
        """
        app = App(self, name)
        # TODO: create implicit network if none is provided?

        log.info('Creating %s...', app)
        # Pull images before creating services
        for service in spec['services']:
            image = spec['services'][service].get('image')
            if not image:
                msg = "Service {}: no image specified".format(service)
                log.error(msg)
                raise ValueError(msg)

            log.debug('Pulling image %s...', image)
            # TODO: check return value
            self.send('pull', image=image)

        try:
            for network in spec.get('networks', {}):
                log.debug('Creating network %s...', network)
                self.create_network(app, network, spec)

            for service in spec['services']:
                log.debug('Creating service %s...', service)
                self.create_service(app, service, spec)

        except APIError as e:
            app.remove()
            raise e

        log.info("%s created", app)
        return app

    def create_network(self, app, name, spec):
        args = self._spec_to_network(name, spec)

        if not args:
            # 'external', return existing network
            network = self.network(name)
            return network

        args['labels'] = args.get('labels') or {}
        args['labels'].update(**{
            'org.lsdsuite.app.id': app.id,
            'org.lsdsuite.app.name': app.name,
        })

        network = self.client.create_network(**args)
        network = self.network(network['Id'])

        return network

    def create_service(self, app, name, spec):
        args = self._spec_to_service(name, spec)
        args['labels'] = args.get('labels') or {}
        args['labels'].update(**{
            'org.lsdsuite.app.id': app.id,
            'org.lsdsuite.app.name': app.name,
        })

        service = self.client.create_service(**args)
        service = self.service(service['ID'])

        return service

    def _spec_to_network(self, name, spec):
        """Converts a YAML spec to an arguments dict
        for docker.APIClient.create_network
        """
        spec = spec['networks'][name]

        if spec.get('external'):
            # external means use existing network
            # 2 different forms:
            # networks:
            #   my-network:
            #     external: true
            #
            # networks:
            #   name-inside-spec:
            #     external:
            #       name: actual-name-of-network
            #
            # TODO: only 1st form is supported for now
            # real_name = spec.get('external', {}).get('name', name)
            # return real_name, name

            return None

        ipam = spec.get('ipam', {})
        pool_configs = []
        for config in ipam.get('config', []):
            subnet = config.get('subnet')
            if subnet:
                config = types.IPAMPool(subnet=subnet)
                pool_configs.append(config)
                # TODO: iprange, gateway, aux_addresses
                # these aren't in the Docker compose spec, but we should make
                # a special exception for lsdsuite-ipam

        ipam = types.IPAMConfig(
            driver=ipam.get('driver', 'default'),
            pool_configs=pool_configs
        )

        return dict(
            name=name,
            driver=spec.get('driver'),
            options=spec.get('driver_opts'),
            ipam=ipam,
            check_duplicate=None,  # TODO: set to True?
            internal=spec.get('internal'),
            # TODO: parse list-of-strings label syntax
            labels=spec.get('labels'),
            # enable_ipv6=spec.get('enable_ipv6'),  # Not supported in Swarm
            attachable=spec.get('attachable'),
            scope='swarm'
        )

    def _spec_to_service(self, name, spec):
        """Converts a YAML spec to an arguments dict
        for docker.APIClient.create_service
        """
        spec = spec['services'][name]
        depl = spec.get('deploy', {})

        mounts = []
        for mount in spec.get('volumes', []):
            if type(mount) is str:
                mount = types.Mount.parse_mount_string(mount)
            else:
                mount = types.Mount(
                    source=mount['source'],
                    target=mount['target'],
                    type=mount.get('type', 'volume'),
                    read_only=mount.get('read_only', False),
                    # TODO: propagation
                    # TODO: no_copy
                    # TODO: labels
                    # TODO: driver_config
                )
            mounts.append(mount)

        ports = []
        for port in spec.get('ports', []):
            if type(port) is str:
                m = PORT_REGEX.match(port)
                if not m:
                    raise ValueError("Can't parse port specification " + port)

                target, published, protocol = m.groups()
                port = dict(
                    target=int(target),
                    published=int(published),
                    protocol=protocol
                )

            port = {
                'Protocol': port.get('protocol'),
                'PublishMode': port.get('mode'),
                'PublishedPort': port.get('published'),
                'TargetPort': port.get('target')
            }
            ports.append(port)

        container_spec = types.ContainerSpec(
            image=spec.get('image'),
            command=spec.get('command'),
            args=spec.get('args'),  # not in compose spec
            hostname=spec.get('hostname'),
            env=spec.get('environment'),
            # TODO: dir=spec.get('working_dir'),
            user=spec.get('user'),
            # TODO: parse list-of-strings label syntax
            labels=spec.get('labels'),
            mounts=mounts,
            stop_grace_period=spec.get('stop_grace_period'),
            # TODO: secrets
            tty=spec.get('tty'),
            # TODO: groups
            # TODO: open_stdin=spec.get('stdin_open'),
            # TODO: healthcheck
            # TODO: hosts=spec.get('extra_hosts'),
            # TODO: dns_config
            # TODO: configs
            # TODO: privileges
        )

        log_driver = types.DriverConfig(
            name='syslog',
            options={'syslog-address': self.log_address,
                     'syslog-format': 'rfc5424micro',
                     'tag': '{{.FullID}}'}
        )

        def parse_cpu(cpu):
            if not cpu:
                return None
            return int(float(cpu) * 10**9)

        def parse_mem(mem):
            # TODO: parse strings like "200M", "0.5G"
            if not mem:
                return None
            return None

        lim = depl.get('resources', {}).get('limits', {})
        res = depl.get('resource', {}).get('reservations', {})
        resources = types.Resources(
            cpu_limit=parse_cpu(lim.get('cpus')),
            mem_limit=parse_mem(lim.get('memory')),
            cpu_reservation=parse_cpu(res.get('cpus')),
            mem_reservation=parse_mem(res.get('memory'))
        )

        restart_policy = depl.get('restart_policy', {})
        restart_policy = types.RestartPolicy(
            # Docker default condition is 'any'
            condition=restart_policy.get('condition', 'none'),
            delay=restart_policy.get('delay'),  # TODO: parse duration
            max_attempts=restart_policy.get('max_attempts'),
            window=restart_policy.get('window')  # TODO: parse duration
        )

        task_template = types.TaskTemplate(
            container_spec=container_spec,
            log_driver=log_driver,
            resources=resources,
            restart_policy=restart_policy
            # TODO: placement
            # TODO: force_update
        )

        service_mode = types.ServiceMode(
            mode=depl.get('mode', 'replicated'),
            replicas=depl.get('replicas')
        )

        endpoint_spec = types.EndpointSpec(
            mode=depl.get('endpoint_mode'),
            ports=ports
        )

        return dict(
            task_template=task_template,
            name=name,
            # TODO: parse list-of-strings label syntax
            labels=spec.get('deploy', {}).get('labels'),
            mode=service_mode,
            # TODO: update_config
            networks=spec.get('networks', []),
            endpoint_spec=endpoint_spec
        )

    def _update_cache(self, cls, data):
        id = data.get('ID') or data.get('Id')
        cache = self._cache[cls]
        if id in cache:
            cache[id].update(**data)
        else:
            cache[id] = cls(self, data)

        return cache[id]


class App(object):
    def __init__(self, engine, name=None):
        import uuid
        self.id = uuid.uuid4().hex

        self.engine = engine
        self.name = name
        self.label_selector = "org.lsdsuite.app.id={}".format(self.id)

    def remove(self):
        log.info('Removing %s...', self)
        for service in self.services:
            service.remove()

        for network in self.networks:
            network.remove()

        log.info('%s removed', self)

    @property
    def networks(self):
        return self.engine.networks(label=self.label_selector)

    @property
    def services(self):
        return self.engine.services(label=self.label_selector)

    def service(self, id=None, name=None):
        service, = self.engine.services(id=id, name=name,
                                        label=self.label_selector) or [None]

        return service

    def __repr__(self):
        return "App[{id}]:{name}".format(id=self.id[:16], name=self.name)


class Resource(dict):
    """Base class for resources such as Tasks, Services, Networks, Node.

    Simply a wrapper around dicts returned by the Docker low-level API.
    """
    def __init__(self, engine, data):
        self.engine = engine
        self.update(**data)

    def reload(self):
        self.engine.get(type(self), self.id)
        return True

    @property
    def id(self):
        return self.get('ID') or self.get('Id')

    @property
    def version(self):
        return self.get('Version', {}).get('Index')

    def __repr__(self):
        return "{type}[{id}]".format(type=self.__class__.__name__,
                                     id=self.id[:16])

    def __eq__(self, other):
        if type(self) == type(other):
            return self.id == other.id
        else:
            return False

    def __hash__(self):
        return hash(self.id)


class Node(Resource):
    """Represents a Docker Swarm Node."""
    @property
    def ip(self):
        return self['Status']['Addr']

    def send(self, command, **params):
        """Sends a command to the lsdsuite-slave instance running on node.

        Returns True if command succeeded, False otherwise
        """
        import json
        import socket

        msg = json.dumps({'command': command, 'params': params})
        log.debug("Send [%s] %s", self.ip, msg)

        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            # TODO: catch "Connection refused" ?
            sock.connect((self.ip, self.engine.slave_port))
            sock.send(msg.encode())

            resp = b''
            while True:
                r = sock.recv(2**10)
                if r:
                    resp += r
                else:
                    break

            resp = resp.decode()
            log.debug("Response [%s] %s", self.ip, resp.strip())
            resp = json.loads(resp)
            return resp.get('status') == 'ok'

        return False

    @property
    def state(self):
        return self['Status']['State']

    @property
    def ready(self):
        return self.state == 'ready'

    @property
    def status(self):
        return self.send('status')

    @property
    def hostname(self):
        return self['Description']['Hostname']

    @property
    def role(self):
        return self['Spec']['Role']

    @property
    def manager(self):
        return self.role == 'manager'

    def __repr__(self):
        return super().__repr__() + ":" + self.ip


class Network(Resource):
    """Represents a Docker Network."""
    @property
    def name(self):
        return self['Name']

    def remove(self):
        self.engine.remove_network(self.id)

    def __repr__(self):
        return super().__repr__() + ":" + self.name


class Service(Resource):
    """Represents a Docker Swarm Service.

    Offers functionality to scale the service up/down, retrieve tasks, ...
    """
    @property
    def name(self):
        return self['Spec']['Name']

    @property
    def tasks(self):
        return self.engine.tasks(service=self.id)

    @property
    def live_tasks(self):
        return [t for t in self.tasks if t.running]

    @property
    def replicas(self):
        return len(self.live_tasks)

    @property
    def desired_replicas(self):
        return self['Spec']['Mode'].get('Replicated', {}).get('Replicas')

    @desired_replicas.setter
    def desired_replicas(self, n):
        if 'Replicated' not in self['Spec']['Mode']:
            log.warning("%s is not replicated, can't change desired_replicas",
                        self)
            return

        # NOTE: Docker re-balances tasks when doing this...

        self.reload()
        log.debug("%s: set desired_replicas from %d to %d",
                  self, self.desired_replicas, n)
        self['Spec']['Mode']['Replicated']['Replicas'] = n
        self.engine.update_service(self)

    @property
    def tasks_by_node(self):
        from collections import defaultdict

        tasks_by_node = defaultdict(list)
        for t in self.live_tasks:
            tasks_by_node[t.node].append(t)

        return dict(tasks_by_node)

    def add(self, n, wait=True):
        """Adds `n` replicas and optionally waits for them to start."""
        log.debug("%s: add %d replicas", self, n)
        old_tasks = set(self.tasks)

        self.desired_replicas += n
        time.sleep(5)  # wait for Docker to register change
        if wait:
            self.wait()

        new_tasks = set(self.tasks)

        return list(new_tasks - old_tasks)

    def kill(self, n, signal='TERM'):
        """Kills `n` tasks with signal `signal`

        Works by selecting the node with the most tasks, selecting a random
        task, and sending a "kill" command.
        """
        import random

        log.debug("%s: Kill %d tasks", self, n)
        self.reload()

        replicas = self.replicas
        if replicas < n:
            log.error("%s: Can't kill %d tasks: only %d are alive",
                      self, n, replicas)
            n = replicas

        tasks_by_node = self.tasks_by_node
        kill_list = []
        for i in range(replicas):
            # find node with most tasks
            tasks = max(tasks_by_node.values(), key=len)
            t = random.choice(tasks)
            kill_list.append(t)
            tasks.remove(t)

        kills = []
        for t in kill_list:
            if len(kills) >= n:
                break

            log.debug("%s: Killing task %d/%d", self, len(kills)+1, n)
            log.debug("Trying to kill %s...", t)

            if t.kill(signal=signal):
                log.debug("%s killed!", t)
                kills.append(t)
            else:
                log.error("Couldn't kill %s", t)

        if len(kills) < n:
            log.error("Could only kill %d/%d tasks", len(kills), n)

        return kills

    def remove(self):
        """Terminates service."""
        self.engine.remove_service(self.id)

    def wait(self, sleep=0.5, max_sleep=None):
        """Waits for tasks to start/terminate.

        Loops until all tasks reached their expected state (and returns True),
        or a delay of max_sleep has been reached (and returns False)
        """
        total_sleep = 0
        while not all(t.ok for t in self.tasks):
            log.debug("%s: wait: %d/%d", self,
                      sum(t.ok for t in self.tasks),
                      len(self.tasks))
            time.sleep(sleep)
            total_sleep += sleep
            if max_sleep and total_sleep >= max_sleep:
                log.debug("%s: wait: timeout", self)
                return False
        else:
            return True

    def __repr__(self):
        return super().__repr__() + ":" + self.name


class Task(Resource):
    """Represents a Docker Swarm Task.

    (i.e. a container running on a node as part of a service)
    """
    @property
    def state(self):
        return self['Status']['State']

    @property
    def desired_state(self):
        return self['DesiredState']

    @property
    def running(self):
        """Is this task running?"""
        return self.state == 'running'

    @property
    def ok(self):
        """Does this task's state match its desired state?"""
        if self.desired_state == 'running':
            return self.state == 'running'
        else:
            return self.state != 'running'

    @property
    def container(self):
        """ID of this task's container"""
        return self['Status']['ContainerStatus'].get('ContainerID')

    @property
    def node_id(self):
        """ID of the node this task is running on"""
        return self.get('NodeID')

    @property
    def node(self):
        """Node this task is running on"""
        id = self.node_id
        if id is None:
            return None
        else:
            return self.engine.node(self.node_id)

    @property
    def service_id(self):
        return self['ServiceID']

    def kill(self, signal='TERM'):
        return self.node.send('kill', id=self.container, signal=signal)

    def wait(self, sleep=0.5, max_sleep=None):
        """Waits until task's desired state matches its state

        Loops until task starts/stops running (and returns True),
        or a delay of max_sleep has been reached (and returns False)
        """
        self.reload()
        total_sleep = 0
        while not self.ok:
            self.reload()
            log.debug("%s: wait: %s/%s", self, self.state, self.desired_state)
            time.sleep(sleep)
            total_sleep += sleep
            if max_sleep and total_sleep >= max_sleep:
                log.debug("%s: wait: timeout", self)
                return False
        else:
            return True

    def __repr__(self):
        return (super().__repr__()
                + "({})".format((self.container or "None            ")[:16])
                + "@" + repr(self.node))
