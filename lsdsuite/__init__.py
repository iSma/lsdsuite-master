DEFAULT_CONFIG = {
    'lsdsuite_dir': "/tmp/lsdsuite",
    'results_dir': "./results",
    'slave_image': "127.0.0.1:5000/lsdsuite/slave",
    'slave_port': 7000,
    'engine': "docker",
    'docker_sock': "/var/run/docker.sock",
    'nodes': [
        {'address': '127.0.0.1'}
    ]
}


def get_ssh(node):
    from paramiko import SSHClient, WarningPolicy

    ssh = SSHClient()
    ssh.load_system_host_keys()
    ssh.set_missing_host_key_policy(WarningPolicy())
    ssh.connect(node.get('address'),
                username=node.get('username'),
                password=node.get('password'),
                key_filename=node.get('private_key'))

    return ssh
