import logging

log = logging.getLogger(__name__)


class Churn(object):
    def __init__(self, spec):
        import operator as op

        def error(name, msg):
            error = "Error in churn specification (service {name}): "
            error += msg
            return error.format(name=name)

        self.start_replicas = {}  # {<service>: <start-replicas>, ...}
        self.timeline = []  # = [(time, service, op, n, signal), ...]

        for name, churn in spec.get('services', {}).items():
            churn = churn['churn']
            if 'synthetic' not in churn and 'real' not in churn:
                msg = error(name,
                            "one of synthetic or real must be specified")
                log.error(msg)
                raise ValueError(msg)

            if 'synthetic' in churn and 'real' in churn:
                msg = error(name,
                            "only one of synthetic or real can be specified")
                log.error(msg)
                raise ValueError(msg)

            if 'synthetic' in churn:
                service = _parse_synthetic(name, churn['synthetic'])
            elif 'real' in churn:
                service = _parse_real(name, churn['real'])

            if 'end' not in service:
                msg = error(name, "no end time specified")
                log.error(msg)
                raise ValueError(msg)

            self.start_replicas[name] = service['start']
            self.timeline += service['steps']
            self.timeline.append((service['end'], name, 'end', 0, None))

        # Sort by time
        self.timeline.sort(key=op.itemgetter(0))

    def apply_start(self, spec):
        services = spec.get('services')
        if not services:
            log.warning("No services specified in spec.")
            return

        for name, n in self.start_replicas.items():
            service = services.get(name)
            if not service:
                log.warning("Service %s in churn doesn't appear in spec.",
                            name)
                continue

            log.debug("Setting service %s start replicas to %d", name, n)
            service['deploy'] = service.get('deploy', {})
            service['deploy']['replicas'] = n

    def start(self, engine, app):
        import json
        import time

        last_t = 0
        n_steps = len(self.timeline)
        total_time = self.timeline[-1][0]
        log.info("Churn steps: %d, duration %ds", n_steps, total_time)
        for i, (t, name, op, n, signal) in enumerate(self.timeline):
            i += 1
            sleep = max(0, t - last_t)
            last_t = t
            log.info("Churn step %d/%d, sleeping for %ds", i, n_steps, sleep)
            time.sleep(sleep)

            service = app.service(name=name)
            if not service:
                log.warning("Churn step %d, unknown service %s", i, name)
                continue

            msg = {
                'type': "churn",
                'step': i,
                'service': name,
                'op': op,
                'num': n
            }

            if type(n) == list:
                # TODO: we don't support killing/adding specific replicas yet.
                # Instead, we're just killing/adding len(replicas) random ones.
                n = len(n)
                msg['num'] = n

            if type(n) == float:
                msg['num'] = str(100*n) + '%'
                n = round(n * service.replicas)

            if op in ['kill', 'add', 'end']:
                engine.send('mark', msg=json.dumps(msg))
            else:
                log.warning("Churn step %d, unknown op %s", i, op)
                return

            if op == 'kill':
                log.info("Churn step %d/%d, killing %d instances of %s",
                         i, n_steps, n, name)
                service.kill(n, signal=signal)
            elif op == 'add':
                log.info("Churn step %d/%d, adding %d instances of %s",
                         i, n_steps, n, name)
                service.add(n, wait=False)
            elif op == 'end':
                log.info("Churn step %d/%d, end of %s", i, n_steps, name)
                service.desired_replicas = 0


def _parse_real(name, spec):
    import sqlite3

    database = spec.get('database')
    if not database:
        raise ValueError("Real churn spec must have database")

    time_factor = spec.get('time_factor', 1)
    time_step = spec.get('time_step', 10)
    max_duration = spec.get('max_duration')
    signal = spec.get('signal')

    sql = sqlite3.connect(database)
    cur = sql.cursor()
    # HUGE performance boost
    cur.execute(r"CREATE INDEX IF NOT EXISTS start_time_index "
                "ON event_trace (event_start_time)")
    cur.fetchone()

    cur.execute(r"SELECT MIN(event_start_time), MAX(event_start_time) "
                "FROM event_trace")
    min_time, max_time = map(int, cur.fetchone())

    cur.execute("""
    SELECT DISTINCT node_id FROM event_trace
    WHERE event_start_time <= ?
    ORDER BY node_id""",
                [max_time])

    node_ids = [x[0] for x in cur.fetchall()]
    nodes = dict(zip(node_ids, range(len(node_ids))))

    end_time = (max_time - min_time) / time_factor / time_step
    end_time = int(end_time + 1) * time_step
    end_time = min(end_time, max_duration)

    service = {'start': 0, 'end': end_time, 'steps': []}

    step = time_step * time_factor
    for i, t in enumerate(range(min_time, max_time+step+1, step)):
        time = (i+1) * time_step
        if max_duration and time > max_duration:
            break

        cur.execute("""
        SELECT node_id, event_type FROM event_trace
        WHERE event_start_time >= ?
        AND event_start_time < ?
        ORDER BY node_id, event_start_time""",
                    (t, t+step))

        events = cur.fetchall()
        kill, add = [], []
        for node in {x[0] for x in events}:
            ups = sum(1 for x in events if x[0] == node and x[1] == 1)
            downs = sum(1 for x in events if x[0] == node and x[1] == 0)

            if ups > downs:
                add.append(nodes[node])
            if downs > ups:
                kill.append(nodes[node])

        if kill:
            service['steps'].append((time, name, 'kill', kill, signal))
        if add:
            service['steps'].append((time, name, 'add', add, None))

    return service


def _parse_synthetic(name, steps):
    def parse_n(n):
        if type(n) == str and n[-1] == '%':
            n = float(n[:-1]) / 100
            if n < 0 or n > 1:
                raise ValueError("N must be between 0% and 100%")
        elif type(n) == float:
            raise ValueError("N must be integer or percentage")
        else:
            n = int(n)
            if n < 0:
                raise ValueError("N must be >= 0")
        return n

    service = {'steps': []}
    for i, step in enumerate(steps):
        try:
            if 'start' in step:
                if any(op in step for op in ['end', 'kill', 'add']):
                    raise ValueError("start can't appear with end, kill, add")
                if 'start' in service:
                    raise ValueError("only one start is allowed")
                if step['start'] < 0:
                    raise ValueError("start must be >= 0")

                service['start'] = step['start']
                continue

            if 'end' in step:
                if any(op in step for op in ['kill', 'add']):
                    raise ValueError("end can't appear with kill or add")
                if 'end' in service:
                    raise ValueError("only one end is allowed")
                if step['end'] < 0:
                    raise ValueError("end must be >= 0")

                service['end'] = step['end']
                continue

            if 'kill' not in step and 'add' not in step:
                raise ValueError("step must have start, end, kill or add")

            if 'end' in service:
                raise ValueError("no further steps allowed after end")

            # TODO: check signal
            signal = step.get('signal')
            time = step.get('time')
            if time is None:
                raise ValueError("time must be specified")

            if 'kill' in step:
                n = parse_n(step['kill'])
                service['steps'].append((time, name, 'kill', n, signal))

            if 'add' in step:
                n = parse_n(step['add'])
                service['steps'].append((time, name, 'add', n, None))

        except ValueError as e:
            error = "Error in churn specification ({name}:{i}): "
            error = error.format(name=name, i=i+1)
            error += str(e)
            raise ValueError(error)

    return service
