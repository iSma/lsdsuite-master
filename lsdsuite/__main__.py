import click
import yaml
import logging

import lsdsuite
from lsdsuite.benchmark import Benchmark
from lsdsuite.engine.docker import Engine


@click.group()
@click.version_option('0.0.1')
@click.option('-v', '--verbose', is_flag=True,
              help="Print debug messages.")
@click.option('--engine', type=click.Choice(['docker']), default='docker',
              help="Container engine.")
@click.option('--config', type=click.File('r'), default="./config.yaml",
              help="Cluster configuration file.")
@click.pass_context
def cli(ctx, **kwargs):
    """LSDSuite.
    TODO
    """
    ctx.obj = kwargs
    ctx.obj['config'] = yaml.load(ctx.obj['config'])

    if ctx.obj['engine'] == 'docker':
        ctx.obj['engine'] = Engine(ctx.obj['config'])

    if ctx.obj['verbose']:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig()
    for log in [lsdsuite.benchmark.log,
                lsdsuite.churn.log,
                lsdsuite.engine.docker.log]:
        log.setLevel(level)


@cli.group()
@click.pass_context
def cluster(ctx):
    """Manages cluster."""


@cluster.command('init')
@click.pass_context
def cluster_init(ctx):
    """Only initialize Swarm manager."""
    engine = ctx.obj['engine']
    click.echo("Initializing Swarm manager")
    engine.cluster_init()
    click.echo("Cluster initialized!")


@cluster.command('up')
@click.option('-f', '--force', is_flag=True,
              help="Force re-initialization.")
@click.pass_context
def cluster_up(ctx, force):
    """Set cluster up."""
    engine = ctx.obj['engine']
    click.echo("Setting up Swarm")
    engine.cluster_up(force=force)
    click.echo("Cluster up!")


@cluster.command('down')
@click.pass_context
def cluster_down(ctx):
    """Tear cluster down."""
    ctx.obj['engine'].cluster_down()
    click.echo("Cluster down!")


@cluster.command('registry')
@click.pass_context
def cluster_registry(ctx):
    """Create private registry service."""
    ctx.obj['engine'].start_registry()
    click.echo("Registry created!")


@cluster.command('status')
@click.pass_context
def cluster_status(ctx):
    """Print cluster status."""
    FORMAT = ("{address:<15} {hostname:<20} {ssh:<8}"
              "{swarm:<8} {ready:<8} {slave:<8} {ok:<8}")

    def print_header():
        click.echo(FORMAT.format(address="ADDRESS",
                                 hostname="HOSTNAME",
                                 ssh="SSH?",
                                 swarm="SWARM?",
                                 ready="READY?",
                                 slave="SLAVE?",
                                 ok="OK?"))

    def print_node(node):
        def unbool(x):
            if x:
                return click.style("OK      ", fg='green')
            else:
                return click.style("NO      ", fg='red')

        node = dict(node)
        for k in ['ssh', 'swarm', 'ready', 'slave', 'ok']:
            if type(node.get(k)) is not str:
                node[k] = unbool(node.get(k))
        node['hostname'] = node.get('hostname') or "???"

        click.echo(FORMAT.format(**node))

    status = ctx.obj['engine'].cluster_status()

    if status is None:
        click.echo("Cluster down, please run 'cluster up'")
        exit(-1)

    click.echo('Cluster status:')
    print_header()
    for node in status:
        print_node(node)

    OK = all(node['ok'] for node in status)

    swarm_nodes = ctx.obj['engine'].nodes()
    config_nodes = ctx.obj['config'].get('nodes', [])
    config_nodes = [n.get('address') for n in config_nodes]
    not_in_config = [n for n in swarm_nodes if n.ip not in config_nodes]

    if not_in_config:
        OK = False
        click.echo("")
        click.echo("Nodes in Swarm but not in config:")
        for node in not_in_config:
            node = {'address': node.ip,
                    'hostname': node.hostname}
            for k in ['ssh', 'swarm', 'ready', 'slave', 'ok']:
                node[k] = "???"

            print_node(node)

    click.echo("")
    if OK:
        click.echo("Cluster ready!")
    else:
        click.echo("Some errors found, please run 'cluster up'")
        exit(-1)


@cli.command('benchmark')
@click.option('--app', type=click.File('r'), required=True,
              help="App description file.")
@click.option('--churn', type=click.File('r'), required=False,
              help="Churn description file.")
@click.option('--runs', type=int, default=1,
              help="Number of benchmark runs.")
@click.option('--name', type=str, required=True,
              help="Benchmark name.")
@click.option('--run-time', type=int, required=False,
              help="Benchmark run duration.")
@click.option('--start-time', type=int, default=10,
              help="Waiting time at start of run.")
@click.option('--end-time', type=int, default=10,
              help="Waiting time at end of run.")
@click.pass_context
def benchmark(ctx, **kwargs):
    """Runs benchmarks."""
    ctx.obj.update(kwargs)

    ctx.obj['app'] = yaml.load(ctx.obj['app'])

    if ctx.obj['churn']:
        ctx.obj['churn'] = yaml.load(ctx.obj['churn'])

    if not ctx.obj['churn'] and not ctx.obj['run_time']:
        raise click.UsageError(
            "Either --churn or --run-time must be specified")

    if ctx.obj['runs'] < 1:
        raise click.UsageError("--runs must be >= 1")

    bench = Benchmark(ctx.obj['engine'], ctx.obj['config'], ctx.obj['name'],
                      ctx.obj['app'], ctx.obj['churn'], ctx.obj['run_time'],
                      ctx.obj['start_time'], ctx.obj['end_time'])

    for _ in range(ctx.obj['runs']):
        try:
            bench.start()
        except KeyboardInterrupt:
            click.echo("Cleaning up before exit...")
            bench.stop(wait=0, get_logs=False)
            exit(1)


@cli.command('prune')
@click.pass_context
def prune(ctx, **kwargs):
    """Prune rogue services and networks."""
    ctx.obj.update(kwargs)
    ctx.obj['engine'].prune()

if __name__ == '__main__':
    cli(prog_name='lsds')
